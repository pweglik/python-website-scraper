from requests import get
from bs4 import BeautifulSoup
import mechanize
import time

time0 = time.time()
cars = [['Peugeot', 'Peugeot']] #there goes all names and short names of cars


for car in cars:
    print("STARTING " + car[1])
    br = mechanize.Browser()
    br.open('https://soldcar.pl/dev/wp-admin/edit-tags.php?taxonomy=carproducer&post_type=car')

    br.form = list(br.forms())[0]
    log = br.form.find_control('log')
    log.value = ''
    pwd = br.form.find_control('pwd')
    pwd.value = ''
    br.submit()

    br.form = list(br.forms())[2]
    name = br.form.find_control('tag-name')
    parent = br.form.find_control('parent')

    html_code = BeautifulSoup(br.response().read(), 'html.parser')
    select = html_code.find('select', id='parent')
    options = select.find_all('option')

    target_car = car[0]
    target_car_to_seek = car[1]
    url = 'https://www.auto-swiat.pl/katalog/' + target_car.lower()
    response = get(url)

    html_soup = BeautifulSoup(response.text, 'html.parser')
    divs = html_soup.find_all('div', class_='listContentWrapper')

    for div in divs:
        anchors = div.find_all('a')
        model = anchors[0].find('h2').text[len(target_car) + 1:]

        name.value = model
        for option in options:
            if option.has_attr('class'):
                if target_car_to_seek == option.text and option['class'][0] == 'level-0':
                    parent.value = [option['value']]
                    break

        br.submit()
        print('Added ' + name.value + ' in ' + parent.value[0])
        br.form = list(br.forms())[2]
        name = br.form.find_control('tag-name')
        parent = br.form.find_control('parent')

    br = mechanize.Browser()
    br.open('https://soldcar.pl/dev/wp-admin/edit-tags.php?taxonomy=carproducer&post_type=car')

    br.form = list(br.forms())[0]
    log = br.form.find_control('log')
    log.value = '' #log and password deleted for safety reasons
    pwd = br.form.find_control('pwd')
    pwd.value = ''
    br.submit()

    br.form = list(br.forms())[2]
    name = br.form.find_control('tag-name')
    parent = br.form.find_control('parent')

    html_code = BeautifulSoup(br.response().read(), 'html.parser')
    select = html_code.find('select', id='parent')
    options = select.find_all('option')

    for div in divs:
        anchors = div.find_all('a')
        model = anchors[0].find('h2').text[len(target_car) + 1:]
        print(target_car_to_seek + ' ' + model)
        if len(anchors[1:-1]) == 5:
            print("CAUTION!!! " + target_car_to_seek + ' ' + model + " has 5 or more generations")
        for anchor in anchors[1:-1]:

            gen = anchor.text[len(target_car + model) + 2:]
            name.value = gen

            producer = 0
            for option in options:
                if target_car_to_seek == option.text:
                    producer = option
                    break

            right_parents = producer.find_next_siblings('option')
            for right_parent in right_parents:
                if model == right_parent.text.strip():
                    parent.value = [right_parent['value']]
                    break

            if parent.value[0] == -1:
                print('Nearly added ' + name.value + ' in ' + parent.value[0])
                time.sleep(100000)
            br.submit()
            print('Added ' + name.value + ' in ' + parent.value[0])
            br.form = list(br.forms())[2]
            name = br.form.find_control('tag-name')
            parent = br.form.find_control('parent')

    timetemp = time.time()
    print('Script running for: ' + str((timetemp - time0) / 60) + " mins")










